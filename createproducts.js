module.exports.createProducts = function () {
    return new Promise((resolve, reject) => {
        let sqlQuery = `create table products (product_id int, product_name varchar(50),category_id int,foreign key(category_id) references categories(category_id) on delete cascade)`;
        connection.query(sqlQuery, (err, result) => {
            if (err) {
                console.log(err, err.stack);
                reject(err);
            } else {
                console.log(result);
                resolve(result);
            }
        });
    });
};