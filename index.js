var mysql = require('mysql');
var products = require('./createproducts');
var category = require('./createcategory');
var insertcategory = require('./insertcategory');
var insertproducts = require('./insertproducts');
var deletecategory = require('./deletecategory');
let mysqlConnectionObj = {
    host: process.env.HOST || "xxxxxxxxxxxxxxxxxxxxxxxxx",
    user: process.env.DBUSER || "xxxxxxxxxxxxx",
    password: process.env.PASSWORD || "xxxxxxxxxxxxxx",
    database: process.env.DATABASE || "xxxxxxxxxxxxxx"
};
console.log("mysql Connection object");
console.log(JSON.stringify(mysqlConnectionObj));
const connection = mysql.createConnection(mysqlConnectionObj);
connection.connect((err) => {
    if (err) {
        console.log(`error in connecting in mysql DB  ${err}`);
    } else {
        console.log(`Connected to the MySql DB ${connection.threadId}`);
        global.connection = connection;
        global.mysqlConnection = connection;
    }
});

// For creating connection object it will take time so i am  Putting this methods in side of setTimeout function  

setTimeout(async function () {
    let createCatresult = await category.createCategoryTable();
    let createProresult = await products.createProductsTable();
    let insertcatresult = await insertcategory.insertCategory();
    let insertprodresut = await insertproducts.insertProducts();
    let result = await deletecategory.deleteCategory();
    console.log('deleted result is ', result);
}, 6000);





